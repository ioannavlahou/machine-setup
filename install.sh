#!/bin/bash -x
set -ueo pipefail

#if [ "$(id -u)" != 0 ]; then
#    sudo -H "$0" "$@"
#    exit $?
#fi
sudo cat  << EOF > /etc/network/interfaces
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo
iface lo inet loopback
EOF 
sudo ifup -a && sudo ifdown -a

if [ ! -f ~/.ssh/bitbucket ]; then
    ssh-keygen -f ~/.ssh/bitbucket -b 2048 -t rsa -q -N "" -C user@stratagem.co
    cat ~/.ssh/bitbucket.pub | xclip -sel clip
    ssh-add ~/.ssh/bitbucket
    
    echo "Copy contents of clipboard into bitbucket SSH keys and then press enter"
    read -n 1 -s
fi

if [ -z ${1+"x"} ]
then
    STRATAGEM_ROOT=~/stratagem
else
    STRATAGEM_ROOT="$1"
fi

echo "Installing in $STRATAGEM_ROOT"

STRATAGEM_REPOS=(research-common
research-data-processing
research-sports-modelling
stratagem-dataprocessing
stratagem-python-utilities
stratagem-trading)


function osx_install(){
   brew ls --versions "$1" > /dev/null || brew install "$@"
}

OSX_INSTALL_COMMAND=osx_install

OSX_INSTALL_PACKAGES=(
    git
    libev

)

LINUX_INSTALL_COMMAND="sudo apt-get -y install "
LINUX_INSTALL_PACKAGES=(
    git
    gcc
    build-essential
    python-dev
    python-pip
    python3-dev
    python3-pip
    cmake
    libxml2-dev
    libxslt-dev
    mesa-common-dev
    qt-sdk
    qt4-qmake
    libev4
    libev-dev
    mysql-server
    libmysqlclient-dev
    libfreetype6-dev
    libpng-dev
    libssl-dev
)

PYTHON_GLOBAL_PACKAGES=(
    "--upgrade pip"
    merge-requirements
    virtualenv
    jupyter
    ipykernel
)

PYTHON_VIRTUALENV_POST_PACKAGES=(
    #"--upgrade --force-reinstall cassandra-driver"
    "--upgrade kombu"
    "--upgrade seaborn"
)

case $OSTYPE in
    darwin*)
        INSTALL_COMMAND=$OSX_INSTALL_COMMAND
        INSTALL_PACKAGES=("${OSX_INSTALL_PACKAGES[@]}")
        ;;
    linux*)
        INSTALL_COMMAND=$LINUX_INSTALL_COMMAND
        INSTALL_PACKAGES=("${LINUX_INSTALL_PACKAGES[@]}")
        ;;

    *)
        echo "$OSTYPE not supported"
        exit 1
esac

mkdir -p "$STRATAGEM_ROOT"
cd "$STRATAGEM_ROOT" || exit 1

for PACKAGE in "${INSTALL_PACKAGES[@]}";
do 
   $INSTALL_COMMAND "$PACKAGE"
done

for REPO in "${STRATAGEM_REPOS[@]}";
do
    if [ ! -d "./$REPO" ];
    then
        git clone "git@bitbucket.org:onsideanalysis/$REPO.git" "./$REPO"
    fi
done

for PACKAGE in "${PYTHON_GLOBAL_PACKAGES[@]}";
do
    sudo pip install $PACKAGE
done

function setup_virtualenv()
{
    cd "$STRATAGEM_ROOT" || exit 1

    rm -f requirements-merged.txt
    touch requirements-output.txt
    for RF in $(find . -name requirements.txt -maxdepth 2);
    do
        merge_requirements <( tr A-Z a-z < requirements-output.txt) <( tr A-Z a-z < $RF)
        mv -f requirements-{merged,output}.txt
    done
    mv -f requirements-{output,merged}.txt

    #TODO: Check the virtualenv exists!!
    if [ ! -d .stratagem_env ]
    then
        virtualenv .stratagem_env
    fi


    set +o nounset
    . .stratagem_env/bin/activate
    set -o nounset

    #Let the faff begin

    pip install numpy==1.8.1
    CFLAGS='-std=c99' pip install gevent==1.0.2
    pip install -r requirements-merged.txt
    

    for PACKAGE in "${PYTHON_VIRTUALENV_POST_PACKAGES[@]}";
    do
        pip install $PACKAGE
    done

    for REPO in "${STRATAGEM_REPOS[@]}";
    do
        pip install -e "./$REPO"
    done

    ipython kernel install --name ".stratagem_env" --user
}

setup_virtualenv

echo "Go to ${STRATAGEM_ROOT} and type . .stratagem_env/bin/activate to activate your environment"
